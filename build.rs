// This build.rs is only used for compiling the frontend protobuf when building with cargo.
// It is not used when building with soong.

fn main() {
    protobuf_codegen::Codegen::new()
        // All inputs and imports from the inputs must reside in `includes` directories.
        .includes(&["src/"])
        // Inputs must reside in some of include paths.
        .input("src/frontend.proto")
        // Specify output directory relative to Cargo output directory.
        .cargo_out_dir("ninja_frontend_protos")
        .run_from_script();
}
